#input user_id
#output json
#output type
#output level
#output date

import groovy.sql.Sql;
import groovy.json.JsonBuilder;

import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null

query = "select type, level, date from alert a"
// + " where a.neutral_user_oid = " +user_id

//println query

def result = null
def jsonString = null

def ris=0
//result = session.createSQLQuery(query).list()
result =  Sql.newInstance("jdbc:mysql://89.121.250.90:3308/smarth2o", "smarth2ouser",
                      "dsmusmarth2o", "com.mysql.jdbc.Driver").rows(query)

                      
def average = 0;

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

	// write to file
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    jGenerator.writeStartArray()

    for (r in result){
    	//write json
    	jGenerator.writeStartObject()
	    jGenerator.writeStringField("type", r[0])
	 	jGenerator.writeNumberField("level", r[1])
	 	jGenerator.writeStringField("date", r[2].toString())
	    jGenerator.writeEndObject()

	}

	jGenerator.writeEndArray()
    jGenerator.close()

    json = out.toString()
   

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

commit(session)

//println json

//println new JsonBuilder(alerts:result).toPrettyString()

//println mapper

return ["json" : new JsonBuilder(alerts:result).toPrettyString()]