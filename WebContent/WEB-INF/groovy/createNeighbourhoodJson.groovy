#input user_id
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null

query = "select b.address, d.city, d.country, u.last_name, u.oid from user u " +
		"left outer join neutral_user nu on u.oid = nu.user_oid " +
		"left outer join household h on nu.household_oid = h.oid " +
		"left outer join building b on b.oid = h.building_oid " +
		"left outer join district d on d.oid = b.district_oid " +
		"where d.zipcode = (select d.zipcode from neutral_user nu " +
		"left outer join household h on nu.household_oid = h.oid " +
		"left outer join building b on b.oid = h.building_oid " +
		"left outer join district d on d.oid = b.district_oid " +
		"where nu.user_oid = " +user_id +")"

//println query

def result=null
def jsonString = null

def ris=0
result = session.createSQLQuery(query).list()
def average = 0;

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    jGenerator.writeStartArray()


    for (r in result){
    	//write json
    	jGenerator.writeStartObject()
	    jGenerator.writeStringField("address", r[0])
	 	jGenerator.writeStringField("city", r[1])
	 	jGenerator.writeStringField("country", r[2])
	 	jGenerator.writeStringField("lastname", r[3])
	 	jGenerator.writeNumberField("oid", r[4])
	    jGenerator.writeEndObject()

	}

	jGenerator.writeEndArray()
    jGenerator.close()

    json = out.toString()

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

commit(session)

//println json
ArrayNode mapper = new ObjectMapper().readTree(json);
//println mapper
return ["json" : mapper]