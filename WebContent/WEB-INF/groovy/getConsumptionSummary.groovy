#input user_id
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null

query = "SELECT monthly_avg, weekly_avg, daily_avg, this_month_total, this_week_total, today_total, first_reading.first_reading_date_time, last_reading.last_reading_date_time FROM " +
		"(SELECT smart_meter_oid, AVG(avrg_daily_average) daily_avg FROM " +
		"(SELECT mr.smart_meter_oid,  " +
		"MAX(mr.total_consumption_adjusted) - MIN(mr.total_consumption_adjusted) avrg_daily_average " + 
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid " +
 		" = h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
 		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		" WHERE user_oid = " + user_id +
 		" GROUP BY mr.smart_meter_oid, DATE(mr.reading_date_time))t  " +
		"GROUP BY  smart_meter_oid) avrg LEFT OUTER JOIN " +
		
		"(SELECT smart_meter_oid, AVG(avrg_monthly_average) monthly_avg FROM " +
		"(SELECT mr.smart_meter_oid, MAX(mr.total_consumption_adjusted)-MIN(mr.total_consumption_adjusted) avrg_monthly_average " + 
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid  " +
 		" = h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid  " +
 		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid  " +
		" WHERE user_oid = " + user_id + 
 		" GROUP BY mr.smart_meter_oid, YEAR(mr.reading_date_time), MONTH(mr.reading_date_time)) m  " +
		"GROUP BY  smart_meter_oid) mavrg ON mavrg.smart_meter_oid = avrg.smart_meter_oid LEFT OUTER JOIN  " +
				
		"(SELECT smart_meter_oid, AVG(avrg_weekly_average) weekly_avg FROM " +
		"(SELECT mr.smart_meter_oid, MAX(mr.total_consumption_adjusted)-MIN(mr.total_consumption_adjusted) avrg_weekly_average " + 
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid  " +
 		" = h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid  " +
 		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid  " +
		" WHERE user_oid = " + user_id + 
 		" GROUP BY mr.smart_meter_oid, year(mr.reading_date_time), week(mr.reading_date_time)) w  " +
		"GROUP BY  smart_meter_oid) wavrg ON wavrg.smart_meter_oid = avrg.smart_meter_oid LEFT OUTER JOIN  " +	
		
		"(SELECT mr.smart_meter_oid, MAX(mr.total_consumption_adjusted)-MIN(mr.total_consumption_adjusted) today_total " +
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid " +
		" = h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid  " +
		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		"WHERE DATE(mr.reading_date_time) = (SELECT DATE(mr.reading_date_time) " +
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid " +
		" = h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		"WHERE user_oid = " + user_id +
		" ORDER BY DATE(mr.reading_date_time) DESC LIMIT 1) and user_oid = " + user_id +
		" GROUP BY mr.smart_meter_oid) today ON today.smart_meter_oid = avrg.smart_meter_oid LEFT OUTER JOIN " +
		
		"(SELECT mr.smart_meter_oid, ifnull(MAX(mr.total_consumption_adjusted)-MIN(mr.total_consumption_adjusted), 0) this_month_total " +
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid " +
		"= h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		"WHERE MONTH(mr.reading_date_time) = (SELECT MONTH(mr.reading_date_time) " +
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid " +
		" = h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		"WHERE user_oid = " + user_id +
		" ORDER BY DATE(mr.reading_date_time) DESC LIMIT 1) and YEAR(mr.reading_date_time) = YEAR(now()) and user_oid = " + user_id +
		" GROUP BY mr.smart_meter_oid) monthly ON monthly.smart_meter_oid = avrg.smart_meter_oid LEFT OUTER JOIN " +
		
		"(SELECT mr.smart_meter_oid, MAX(mr.total_consumption_adjusted)-MIN(mr.total_consumption_adjusted) this_week_total " +
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid " +
		" = h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		"WHERE WEEK(mr.reading_date_time) = (SELECT WEEK(mr.reading_date_time) " +
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid " +
		" = h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		"WHERE user_oid = " + user_id +
		" ORDER BY DATE(mr.reading_date_time) DESC LIMIT 1) and YEAR(mr.reading_date_time) = YEAR(now()) and user_oid = " + user_id + 
		" GROUP BY mr.smart_meter_oid) weekly ON weekly.smart_meter_oid = avrg.smart_meter_oid LEFT OUTER JOIN " +
		
		"(SELECT mr.smart_meter_oid, mr.reading_date_time first_reading_date_time " +
		"FROM meter_reading mr   " +
		"LEFT OUTER JOIN household h ON mr.smart_meter_oid = h.smart_meter_oid " +
		"LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
        "LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		"WHERE u.oid = " + user_id +
		" ORDER BY mr.reading_date_time LIMIT 1) first_reading ON first_reading.smart_meter_oid = avrg.smart_meter_oid LEFT OUTER JOIN " + 
		
		"(SELECT mr.smart_meter_oid, mr.reading_date_time last_reading_date_time " +  
		"FROM meter_reading mr " +  
		"LEFT OUTER JOIN household h ON mr.smart_meter_oid = h.smart_meter_oid " +   
		"LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +   
        "LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +   
		"WHERE u.oid = " + user_id +
		" ORDER BY mr.reading_date_time DESC LIMIT 1) last_reading ON last_reading.smart_meter_oid = avrg.smart_meter_oid" 

println query

def result=null
def jsonString = null

def ris=0
result = session.createSQLQuery(query).list()
def average = 0;

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    jGenerator.writeStartArray()


    for (r in result){
    	//write json
    	jGenerator.writeStartObject()
	    jGenerator.writeNumberField("monthly_avg", new Double(r[0]).doubleValue())
	    jGenerator.writeNumberField("weekly_avg", new Double(r[1]).doubleValue())
	 	jGenerator.writeNumberField("daily_avg", new Double(r[2]).doubleValue())
	 	jGenerator.writeNumberField("this_month_total", new Double(r[3]).doubleValue())
	 	jGenerator.writeNumberField("this_week_total", new Double(r[4]).doubleValue())
	 	jGenerator.writeNumberField("today_total", new Double(r[5]).doubleValue())
	 	jGenerator.writeStringField("from", r[6].toString())
	 	jGenerator.writeStringField("to", r[7].toString())
	    jGenerator.writeEndObject()

	}

	jGenerator.writeEndArray()
    jGenerator.close()

    json = out.toString()

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

commit(session)

println json
ArrayNode mapper = new ObjectMapper().readTree(json);
println mapper
return ["json" : mapper]